// JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
const jwt = require("jsonwebtoken");

/*
ANATOMY 
Header - type of token and signing algorithm 
Payload - contains the claims (id,email,isAdmin)
Signature - generate by putting the encoded header, the encoded payload and applying the algorithm in the header
*/

// Used in the algorithm for encrypting our data which makes it difficult to decode the information without defined secret keyword
const secret = "CourseBookingAPI";

// Token Creation
module.exports.createAccessToken = (user) => {
	// payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	// Generate a JSON web token, we are using the jwt's sign method
	return jwt.sign(data, secret, {});
};

